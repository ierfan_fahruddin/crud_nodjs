const path = require('path');
const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser');
// const {urlencoded} = require('body-parser');
const mysql = require('mysql');
const app = express();

const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'crudnode'
});

conn.connect((err) =>{
    if(err)throw err;
    console.log('mysql connected..')
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(bodyParser.json());
// app.use(bodyParser, urlencoded({ extended: true }));

// app.use(bodyParser.urlencoded());
app.use(('/ASSET', express.static(__dirname + '/public')));

app.get('/', (req, res)=> {
    let sql = "select * from profile";
    let query = conn.query( sql, (err, result) => {
        if(err) throw err;
        res.render('./profile/index', {
            result: result
        })
    })
});
app.get('/read', (req, res)=> {
    let sql = "select * from profile";
    let query = conn.query( sql, (err, result) => {
        if(err) throw err;
        res.render('./profile/read', {
            result: result
        })
    })
});
app.post('/save', (req, res) => {
    let data = {nama: req.body.nama, usia: req.body.usia};
    let sql = "insert into profile set ?";
    let query =  conn.query(sql, data,(err, result) => {
        if(err) throw err;
        res.redirect('/');
    })
});

app.post('/update', (req, res) => {
    let sql = "update profile set nama='"+req.body.nama+"', usia='"+req.body.usia+"' where id="+req.body.id;
    let query = conn.query(sql, (err, result) => {
        if(err) throw err;
        res.redirect("/");
    })
});

app.get('/delete/:id', (req, res) => {
    let sql = "delete from profile where id="+req.params.id+"";
    let query = conn.query(sql, (err, result) => {
        if(err) throw err;
        res.redirect('/');
    })
});
app.get('/api/', (req, res)=> {
    let sql = "select * from profile";
    let query = conn.query( sql, (err, result) => {
        if(err) throw err;
        
        res.send(JSON.stringify({"status": 200, "error":null,"response":result}));
    })
});
app.get('/api/:id', (req, res)=> {
    let sql = "select * from profile where id="+req.params.id;
    let query = conn.query( sql, (err, result) => {
        if(err) throw err;
        
        res.send(JSON.stringify({"status": 200, "error":null,"response":result}));
    })
});
app.post('/api/save', (req, res) => {
    let data = {nama: req.body.nama, usia: req.body.usia};
    let sql = "insert into profile set ?";
    let query =  conn.query(sql, data,(err, result) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error":null,"response":result}));
    })
});

app.put('/api/update/:id', (req, res) => {
    let sql = "update profile set nama='"+req.body.nama+"', usia='"+req.body.usia+"' where id="+req.params.id;
    let query = conn.query(sql, (err, result) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error":null,"response":result}));
    })
});

app.get('/api/delete/:id', (req, res) => {
    let sql = "delete from profile where id="+req.params.id+"";
    let query = conn.query(sql, (err, result) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error":null,"response":result}));
    })
});

app.listen(8000, () => {
    console.log('server berjalan di port 8000')
});

// *************************************************************
// *************************************************************
// **	    ***  ********  *******   ***      **     **       **
// **	    ***  **	   **  ******	** **     ** *   **		  **
// **	    ***  ********  **      **   **	  ** * * **       **
// **  	    ***  *****	   ****** *********   **  * ***       **
// **  	    ***  **  **    **    **       **  **    ***       **
// **  	    ***  **   **   **   **         ** **     **       **
// *************************************************************
// *************************************************************
